﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PingerAppLibrary.Enums;
using PingerAppLibrary.Implementations;
using PingerAppLibrary.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace PingerAppTests
{
    [TestClass]
    public class PingerAppLibraryTest
    {
        [TestMethod]
        public void PollIcmpOk()
        {
            //arrange
            IEndPoint endPoint = new EndPoint();
            endPoint.Host = "127.0.0.1";
            endPoint.Interval = 1;
            endPoint.Protocol = ProtocolVersion.Icmp;

            IPoll poll = new PoolIcmp();

            const AnswerOptions expected = AnswerOptions.Ok;

            //act
            var actual = poll.PoolEndPoint(endPoint);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PollHttpOk()
        {
            //arrange
            IEndPoint endPoint = new EndPoint();
            endPoint.Host = "https://www.google.com/";
            endPoint.Interval = 1;
            endPoint.Protocol = ProtocolVersion.Http;

            IPoll poll = new PoolHttp(200);

            const AnswerOptions expected = AnswerOptions.Ok;

            //act
            var actual = poll.PoolEndPoint(endPoint);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PollTcpOk()
        {
            //arrange
            IEndPoint endPoint = new EndPoint();
            endPoint.Host = "google.com";
            endPoint.Interval = 1;
            endPoint.Protocol = ProtocolVersion.Tcp;

            IPoll poll = new PoolTcp();

            const AnswerOptions expected = AnswerOptions.Ok;

            //act
            var actual = poll.PoolEndPoint(endPoint);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LoggerTxt()
        {
            //arrange
            const string testLog = "test_log.txt";            
            ILogger logger = new LoggerTxt(testLog);
            IEndPoint endPoint = new EndPoint();
            endPoint.Host = "127.0.0.1";
            endPoint.Interval = 1;
            endPoint.Protocol = ProtocolVersion.Icmp;

            const int expected = 1;

            //act
            File.Delete(testLog);
            logger.WriteLog(endPoint, AnswerOptions.Ok);
            Thread.Sleep(1100);
            var actual = File.ReadAllLines(testLog).Length;

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DattaJson()
        {
            //arrange
            const string endpoints = "[{\"host\":\"ya.ru\",\"interval\":\"1\",\"protocol\":\"ICMP\"}]";
            const string testJson = "test_endpoints.json";

            IEndPoint expected = new EndPoint
            {
                Host = "ya.ru",
                Interval = 1,
                Protocol = ProtocolVersion.Icmp
            };

            IEndPoint actual = null;            

            IData<ICollection<IEndPoint>> data = new DataJson<ICollection<IEndPoint>>(testJson);

            //act
            File.WriteAllText(testJson, endpoints);
            var actualCollection = data.LoadEndPoints();

            foreach (var item in actualCollection)
            {
                actual = item;
            }         

            //assert            
            Assert.AreEqual(expected.Host, actual.Host);
            Assert.AreEqual(expected.Interval, actual.Interval);
            Assert.AreEqual(expected.Protocol, actual.Protocol);
        }
    }
}
