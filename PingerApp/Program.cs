﻿using Autofac;
using PingerAppLibrary.Implementations;
using PingerAppLibrary.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;

namespace PingerApp
{
    public class Program
    {
        public static IContainer IoC { get; set; }
        public static bool PingStarted = false;

        public static void Main(string[] args)
        {
            var controller = InitController();
            Console.WriteLine("Write: start - To ping start; stop - To ping stop; exit - To close application");

            while (true)
            {
                var command = Console.ReadLine();

                switch (command)
                {
                    case "start":
                        StartPing(controller);
                        break;
                    case "stop":
                        PingStop(controller);
                        break;
                    case "exit":
                        PingStop(controller);
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }

        public static void PingStop(IController controller)
        {
            if (!PingStarted)
            {
                Console.WriteLine("Ping already stopped");
            }
            else
            {
                controller.StopPolling();
                Console.WriteLine("Ping stopped");
                PingStarted = false;
            }
        }

        public static void StartPing(IController controller)
        {
            if (PingStarted)
            {
                Console.WriteLine("Ping already started");
            }
            else
            {
                controller.StartPolling();
                Console.WriteLine("Ping started");
                PingStarted = true;
            }
        }

        public static IController InitController()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<EndPoint>().As<IEndPoint>();
            builder.RegisterType<DataJson<List<EndPoint>>>().As<IData<ICollection>>();
            builder.RegisterType<LoggerTxt>().As<ILogger>();
            builder.RegisterType<Controller>().As<IController>();
            IoC = builder.Build();

            IController controller = null;
            using (var scope = IoC.BeginLifetimeScope())
            {
                try
                {
                    controller = scope.Resolve<IController>();
                    var collection = scope.Resolve<IData<ICollection>>().LoadEndPoints();
                    controller.PollCollection(collection);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return controller;
        }
    }
}
