﻿using PingerAppLibrary.Enums;

namespace PingerAppLibrary.Interfaces
{
    public interface ILogger
    {
        void WriteLog(IEndPoint point, AnswerOptions answer);
        void WriteLogError(IEndPoint point, string message);
    }
}
