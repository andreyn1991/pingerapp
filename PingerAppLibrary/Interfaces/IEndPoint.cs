﻿using PingerAppLibrary.Enums;

namespace PingerAppLibrary.Interfaces
{
    public interface IEndPoint
    {
        string Host { get; set; }
        int Interval { get; set; }
        ProtocolVersion Protocol { get; set; }
    }
}
