﻿using PingerAppLibrary.Enums;

namespace PingerAppLibrary.Interfaces
{
    public interface IPoll
    {
        AnswerOptions PoolEndPoint(IEndPoint point);
    }
}
