﻿namespace PingerAppLibrary.Interfaces
{
    public interface IData<out T>
    {
        T LoadEndPoints();
    }
}
