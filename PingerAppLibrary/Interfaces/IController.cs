﻿using System.Collections;

namespace PingerAppLibrary.Interfaces
{
    public interface IController
    {
        void PollCollection(ICollection points);
        void StartPolling();
        void StopPolling();
    }
}
