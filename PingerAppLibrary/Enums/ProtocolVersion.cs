﻿namespace PingerAppLibrary.Enums
{
    public enum ProtocolVersion
    {
        Icmp,
        Http,
        Tcp
    }
}
