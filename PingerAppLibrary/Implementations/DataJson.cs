﻿using Newtonsoft.Json;
using PingerAppLibrary.Interfaces;
using System;
using System.IO;

namespace PingerAppLibrary.Implementations
{
    public class DataJson<T> : IData<T>
    {
        private readonly string _jsonPath;

        public DataJson()
        {
            _jsonPath = "endpoints.json";
        }
        public DataJson(string jsonPath)
        {
            _jsonPath = jsonPath;
        }

        public T LoadEndPoints()
        {
            if (File.Exists(_jsonPath))
            {
                return JsonConvert.DeserializeObject<T>(File.ReadAllText(_jsonPath));
            }
            else
            {
                throw new Exception("Файл со списком опрашиваемых узлов не найден. Добавьте файл endpoints.json в папку с .exe файлом.");
            }
        }
    }
}
