﻿using PingerAppLibrary.Enums;
using PingerAppLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PingerAppLibrary.Implementations
{
    public class LoggerTxt : ILogger
    {
        private readonly string _logPath;
        private readonly List<string> _logList;

        public LoggerTxt()
        {
            _logPath = "log.txt";
            _logList = new List<string>();
            CommitLogAsync();
        }
        public LoggerTxt(string logPath)
        {
            _logPath = logPath;
            _logList = new List<string>();
            CommitLogAsync();
        }

        private void CommitLog()
        {
            while(true)
            {
                if (_logList.Count > 0)
                {
                    File.AppendAllLines(_logPath, _logList);
                    _logList.Clear();
                }
                Thread.Sleep(1000);
            }
        }

        private async void CommitLogAsync()
        {
            await Task.Run(() => CommitLog());
        }        

        public void WriteLog(IEndPoint point, AnswerOptions answer)
        {
            _logList.Add(DateTime.Now + " " + point.Host + " " + answer);
        }

        public void WriteLogError(IEndPoint point, string message)
        {
            _logList.Add(DateTime.Now + " " + point.Host + " " + message);
        }
    }
}
