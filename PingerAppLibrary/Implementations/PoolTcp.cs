﻿using PingerAppLibrary.Enums;
using PingerAppLibrary.Interfaces;
using System.Net;
using System.Net.Sockets;

namespace PingerAppLibrary.Implementations
{
    public class PoolTcp : IPoll
    {
        public AnswerOptions PoolEndPoint(IEndPoint point)
        {
            Socket socket = null;
            AnswerOptions answer;
            var hostEntry = Dns.GetHostEntry(point.Host);

            foreach (var address in hostEntry.AddressList)
            {
                var ipe = new IPEndPoint(address, 80);
                socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                socket.Connect(ipe);

                if (socket.Connected)
                {
                    break;
                }
            }

            if (socket != null && socket.Connected)
            {
                socket.Disconnect(false);
                answer = AnswerOptions.Ok;
            }
            else
            {
                answer = AnswerOptions.Failed;
            }

            return answer;
        }
    }
}
