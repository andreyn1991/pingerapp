﻿using PingerAppLibrary.Enums;
using PingerAppLibrary.Interfaces;
using System.Net.Http;

namespace PingerAppLibrary.Implementations
{
    public class PoolHttp : IPoll
    {
        private readonly int _validStatusCode;
        

        public PoolHttp(int validStatusCode)
        {
            _validStatusCode = validStatusCode;
        }

        public AnswerOptions PoolEndPoint(IEndPoint point)
        {
            AnswerOptions answer;
            using (var client = new HttpClient())
            {
                using (var response = client.GetAsync(point.Host).Result)
                {
                    answer = (int) response.StatusCode == _validStatusCode ? AnswerOptions.Ok : AnswerOptions.Failed;
                }
            }

            return answer;
        }
    }
}
