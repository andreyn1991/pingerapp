﻿using PingerAppLibrary.Enums;
using PingerAppLibrary.Interfaces;
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;

namespace PingerAppLibrary.Implementations
{
    public class Controller : IController
    {
        public ILogger Logger { get; set; }

        private ICollection _points;
        private bool _isPing;

        public Controller(ILogger logger)
        {
            Logger = logger;
        }

        private void PollExecute(IEndPoint point)
        {
            var poll = GetPoll(point);
            var lastAnswer = AnswerOptions.None;

            while (_isPing)
            {
                try
                {
                    var answer = poll.PoolEndPoint(point);
                    if (answer != lastAnswer)
                    {
                        lastAnswer = answer;
                        Logger.WriteLog(point, answer);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteLogError(point, e.Message);
                    break;
                }

                Thread.Sleep(point.Interval * 1000);
            }
        }

        private static IPoll GetPoll(IEndPoint point)
        {
            IPoll poll = null;
            switch (point.Protocol)
            {
                case ProtocolVersion.Icmp:
                    poll = new PoolIcmp();
                    break;
                case ProtocolVersion.Http:
                    poll = new PoolHttp(200);
                    break;
                case ProtocolVersion.Tcp:
                    poll = new PoolTcp();
                    break;
                default:
                    throw new Exception("Не объявлена реализация для выбраного протокола.");
            }

            return poll;
        }

        private async void PollExecuteAsync(IEndPoint point)
        {
            await Task.Run(() => PollExecute(point));
        }

        public void PollCollection(ICollection points)
        {
            _points = points;
        }

        public void StartPolling()
        {
            _isPing = true;
            foreach (IEndPoint point in _points)
            {
                PollExecuteAsync(point);
            }
        }

        public void StopPolling()
        {
            _isPing = false;
        }
    }
}
