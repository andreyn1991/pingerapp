﻿using PingerAppLibrary.Enums;
using PingerAppLibrary.Interfaces;
using System.Net.NetworkInformation;

namespace PingerAppLibrary.Implementations
{
    public class PoolIcmp : IPoll
    {
        public AnswerOptions PoolEndPoint(IEndPoint point)
        {
            PingReply pingReply;
            using (var ping = new Ping())
            {
                pingReply = ping.Send(point.Host);
            }

            var answer = pingReply != null && pingReply.Status == IPStatus.Success
                ? AnswerOptions.Ok
                : AnswerOptions.Failed;


            return answer;
        }
    }
}
