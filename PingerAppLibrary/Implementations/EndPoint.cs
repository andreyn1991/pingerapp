﻿using PingerAppLibrary.Enums;
using PingerAppLibrary.Interfaces;

namespace PingerAppLibrary.Implementations
{
    public class EndPoint : IEndPoint
    {
        public string Host { get; set; }
        public int Interval { get; set; }
        public ProtocolVersion Protocol { get; set; }
    }
}
